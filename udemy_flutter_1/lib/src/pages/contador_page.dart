import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return _ContadorPageState(

    );
  }

}

class _ContadorPageState extends State<ContadorPage>{
  final _estiloTexto = new TextStyle(fontSize: 23.0);
  int _contador    = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Stateful"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Numero de taps",
                style:  _estiloTexto),
            Text(_contador.toString(),
                style:  _estiloTexto)
          ],
        ),
      ),
      floatingActionButton: _crearBotones()
    );
  }

  Widget _crearBotones(){

    return Row(
      children: <Widget>[
        SizedBox(width: 25),
        FloatingActionButton(child: Icon(Icons.restore),onPressed: _reiniciar),
        Expanded(child: SizedBox(),),
        FloatingActionButton(child: Icon(Icons.remove_circle),onPressed: _quitar),
        SizedBox(width: 5.0),
        FloatingActionButton(child: Icon(Icons.add_circle_outline),onPressed: _agregarBtn)

      ],
    );
  }
  void _agregarBtn(){
    setState(()=> _contador++);
  }

  void _quitar(){
    setState(()=> _contador--);
  }

  void _reiniciar(){
    setState(()=> _contador=0);
  }
}

