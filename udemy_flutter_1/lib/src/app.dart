import 'package:flutter/material.dart';
//import '../src/pages/home_page.dart';
import 'package:udemy_flutter_1/src/pages/home_page.dart';


class MyApp extends StatelessWidget{
  @override
  Widget build(context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Center(
            child: HomePage()
        )
    );
  }

}