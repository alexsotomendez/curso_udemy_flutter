import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _AnimatedContainerPageState();

}

class _AnimatedContainerPageState extends State<AnimatedContainerPage>{
  double _width   = 50.0;
  double _height  = 50.0;
  Color  _color   = Colors.pinkAccent;

  BorderRadiusGeometry _borderRadiusGeometry  = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animatedddd"),
      ),
      body: Center(
        child: AnimatedContainer(
          duration: Duration(seconds: 2),
          curve: Curves.fastLinearToSlowEaseIn,
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            color: _color,
            borderRadius: _borderRadiusGeometry
          ),

        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: _cambiarEstado),
    );
  }

   void _cambiarEstado(){
    final random  = Random();
    
    setState(() {
      _width   =  random.nextInt(300).toDouble();
      _height  =  random.nextInt(300).toDouble();
      _color   = Color.fromRGBO(
          random.nextInt(255),
          random.nextInt(255),
          random.nextInt(255),
          1);
      _borderRadiusGeometry = BorderRadius.circular(random.nextInt(100).toDouble());
    });
  }
}