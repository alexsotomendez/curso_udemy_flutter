import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Avatar Page"),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('AS'),
              backgroundColor: Colors.red,
            ),
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: CircleAvatar(
              backgroundImage: NetworkImage('https://manschile.files.wordpress.com/2009/09/jmc.jpg'),
              radius: 25.0,
            ),
          )
          
        ],
        
      ),
      body: Center(
        child: FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'),
            image: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Jos%C3%A9_Miguel_Carrera.jpg/426px-Jos%C3%A9_Miguel_Carrera.jpg')),
      ),
    );
  }
}