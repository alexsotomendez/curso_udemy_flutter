import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/provider/menu_provider.dart';
import 'package:flutter/material.dart';
import 'package:componentes/src/utils/icono_string_util.dart';

class HomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
       title: Text("Componentes"),
      ),
      body: _lista(),
    );
  }

  Widget _lista(){
    //menuProvider.cargarData()
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot){
        return ListView(
        children: _listaItem(snapshot.data, context),
        );
      },
    );
  //  return ListView(
  //    children: _listaItem(),
  //  );
  }
  
  List<Widget> _listaItem(List<dynamic> data, BuildContext context){
    final List<Widget> opciones = [];
    
    data.forEach((opt){
      final widgetTemp = ListTile(
        title: Text(opt['texto']),
        leading: getIcon(opt['icon']),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: (){
          Navigator.pushNamed(context, opt['ruta']);

//          final route = MaterialPageRoute(
//            builder: (context)=>AlertPage()
//          );
//          Navigator.push(context, route);
        },
      );

      opciones..add(widgetTemp)
              ..add(Divider());

    });

    return opciones;
  }

}